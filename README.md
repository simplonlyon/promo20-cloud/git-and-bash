
# Git and Bash

  
  

## À préparer avant 13h45 :

  

* Une VM Debian avec une IP publique

* Créer un compte "simplon" avec le mot de passe de votre choix

* Ajouter le compte "simplon" au groupe "root" et au groupe "wheel"

* Si votre ordinateur fonctionne sous Windows, télécharger putty ici : https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.77-installer.msi . Si vous êtes sous Linux ou sous Mac, vous pourrez vous connecter en lançant la commande `ssh simplon@IP_PUBLIQUE_DE_VOTRE_VM` dans un terminal

* Utiliser putty pour vous connecter sur votre VM à travers son IP publique, avec le compte simplon

* Une fois connecté, devenir root avec `su -` (il **faut** rajouter le tiret à la fin de la commande), et saisir le mot de passe root de votre VM
* Installer les packages `tmux`, `vim`, `git`, et `sudo`
* Ajouter le compte "simplon" à la liste des sudoers (il faut mettre le compte simplon dans le groupe sudo)
* Quitter le compte root avec la commande `exit`
* Se déconnecter de la VM, puis se reconnecter pour récupérer les nouveaux droits
* Avec le compte simplon, créer le dossier `~/.ssh` .
* Créez une paire de clés SSH avec la commande `ssh-keygen -t ed25519 -f ~/.ssh/gitlab` . Ne mettez pas de passphrase à votre clé (il suffit d'appuyer sur Entrée lorsque ssh-keygen vous demande une passphrase)
* Affichez la clé publique avec la commande `cat ~/.ssh/gitlab.pub` . **ATTENTION**, on affiche bien la clé **PUBLIQUE**, celle dont l'extension est `.pub`
* Se connecter à https://gitlab.com/ avec votre compte, puis cliquez sur votre icône de profil en haut à droite de l'écran, puis sur "Préférences"
* Cliquez sur "SSH Keys" dans le menu de gauche
* Collez votre clé publique (elle commence par `ssh-ed25519` et se termine par `simplon@....`) dans la case prévue à cet effet
* Vérifiez que la configuration a bien fonctionné en lançant la commande `ssh -T -i ~/.ssh/gitlab git@gitlab.com` . Vous devez avoir un message `Welcome to gitlab, @<USER>` qui s'affiche.
* Utilisez `nano` pour créer le fichier qui va permettre de se connecter automatiquement
* Commande : `nano ~/.ssh/config`
* Copiez le contenu exact du fichier : 
```
Host gitlab.com
    Hostname gitlab.com
    User git
    Port 22
    IdentityFile ~/.ssh/gitlab
```
  * Vérifiez que la configuration a fonctionné, avec la commande `ssh -T gitlab.com` . Le même message de bienvenue que précédemment doit s'afficher.


## Checklist : 


  * J'ai créé ma VM Debian : 
  * J'ai créé le compte simplon :
  * J'arrive à me connecter à ma VM avec un vrai client SSH : 
  * J'ai installé git, tmux, vim, et sudo :
  * J'ai créé mon dossier .ssh :
  * J'ai créé mes clés SSH :
  * J'ai mis ma clé publique sur Gitlab.com et j'ai testé que ça fonctionne :
  * J'ai créé mon fichier de configuration et j'ai testé que ça fonctionne :
